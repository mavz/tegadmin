import Vue from 'vue';
import Vuex from 'vuex';

import Axios from 'axios';

import VueJWT from 'vuejs-jwt'
Vue.use(VueJWT);

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        auth_data: localStorage.getItem('access_token') || null,
        user: localStorage.getItem('access_token') ? Vue.$jwt.decode(localStorage.getItem('access_token')) : null,
        refresh_token: localStorage.getItem('refresh_token') || null,
    },

    getters: {
        AUTH_DATA: state => {
           return 'admin';
        },
        USER_DATA: state => {
            return state.user;
        },
        USER_ROLE: state => {
            return 'admin';
        },
        GET_EXP: state  => {
            if(state.user == null) 
                return null
            return state.user.exp;
        }
    },

    mutations: {
        SET_AUTH_DATA: (state, payload) => {
           state.auth_data =  payload.access_token;
           state.user = Vue.$jwt.decode(payload.access_token);
           state.refresh_token = payload.refresh_token;
        },
        DELETE_AUTH_DATA: (state, payload) => {
            state.auth_data = null;
            window.location.href = '/auth';
        }
    },

    actions: {
        SAVE_AUTH_DATA: (context, payload) => {
            localStorage.setItem('access_token', payload.access_token);
            localStorage.setItem('refresh_token', payload.refresh_token);
            context.commit('SET_AUTH_DATA', payload);
        },
        AUTH_LOGOUT: (context, payload) => {
            localStorage.removeItem('access_token');
            Axios.post('/auth/logout');
            context.commit('DELETE_AUTH_DATA', payload);
        },
        async AUTH_REFRESH(context) {
            if (localStorage.getItem('refresh_token') == ''){   
                window.location.href = '/';
            }
            const str = {
                refresh_token: localStorage.getItem('refresh_token'),
              };
            const payload = JSON.stringify(str);
            const tokens = await axios.post('/auth/refresh-token', payload,  {skipAuth: true})
            context.dispatch('SAVE_AUTH_DATA', tokens.data);
            return context.state.auth_data;
        }
    },
});