var config = require('./config.js');

window._ = require('lodash');
// window.moment = require('vue-moment');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

// try {
//     window.Popper = require('popper.js').default;
//     window.$ = window.jQuery = require('jquery');
//
//     require('bootstrap');
// } catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

//window.axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.access_token}`;
//window.axios.defaults.headers.common['Authorization'] = `Bearer 123`;
//window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.baseURL = config.apiUrl;
window.statApi = {
    url: config.statApiUrl,
    token: config.statApiToken,
    storageUrl: config.statStorageUrl,
};

window.pusherConfig = config.pusher;
/*
window.axios.interceptors.response.use(undefined, (error) => {
    if (error.response && error.response.status === 401) {
        window.location.href = '/';
        localStorage.access_token = '';
        localStorage.refresh_token = '';
    }
    return Promise.reject(error);
});
*/