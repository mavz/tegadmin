module.exports = {
    apiUrl: 'http://localhost:8082',
    statApiUrl: '',
    statApiToken: '1A2B3C4D5A6B7C8D9A0B1C2D3A4B5C6D',
    statStorageUrl: 'http://example.loc/storage',
    pusher: {
        apiKey: '1a2b3c4d5e6f1a2b3c4b5',
        cluster: 'eu'
    }
};