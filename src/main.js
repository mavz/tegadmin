require('./bootstrap');
import 'bootstrap/dist/css/bootstrap.css';
import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';
import VueJWT from 'vuejs-jwt'
Vue.use(VueJWT);

import {store} from './store'
import App from './App.vue';
import router from './router';

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);


// запросить валидный аксесс токен
async function requestValidAccessToken() {
  // сначала запоминаем текущий accessToken из хранилища
  var accessToken = localStorage.getItem('access_token');
  const exp = store.getters.GET_EXP;
  // приводим текущее время к unix timestamp
  const now = Math.floor(Date.now() * 0.001);
  // если accessToken устарел, обновляем его и запоминаем
  if(exp == null || now > exp){
    //console.log('Заправшиваем обновление токена');
    if(localStorage.getItem('refresh_token') == ''){
      //console.log('нет токена рефреш, и мы перенапрявляем на регистрацию');
      router.replace('/auth');
    }
    else{
       accessToken = await requestRefreshToken();
    }
  }
  // возвращаем рабочий accessToken
  return accessToken;
  //return 123;
}

async function requestRefreshToken() {
  return await store.dispatch('AUTH_REFRESH');
}

/**/
/**/ 
// обрабатываем запрос перед отправкой
window.axios.interceptors.request.use(async (config) => {
  //console.log("access_token перед запросом", localStorage.getItem('access_token'));
  //console.log("refresh_token перед запросом",localStorage.getItem('refresh_token'));
  //console.log("config.skipAuth: ", config);
  //return config;
  // если указан флаг skipAuth, пропускаем запрос дальше как есть
  // этот флаг указан у методов login и refreshToken, они не подкрепляются токенами
  //alert("Запрос перед отправкой");
  //console.log(this.$store.dispatch('IS_ACCESS_TOKEN_VALID'));
  if (config.skipAuth) {
    return config;
  }
  // иначе запрашиваем валидный accessToken
  const accessToken = await requestValidAccessToken();
  //console.log("access_token На момент запроса: ", localStorage.getItem('access_token'));
  //console.log("const accessToken На момент запроса: ", accessToken);
  // и возвращаем пропатченный конфиг с токенов в заголовке
  return {
    ...config,
    headers: {
      common: {
        ['Authorization']: `Bearer ${accessToken}`,
      },
    },
  };
});
/**/

window.axios.interceptors.response.use(undefined, (error) => {
  if (error.response && error.response.status === 401) {
      router.replace('/access_denied');
      //console.log("access_denied router");
      //alert("Зарегистрируйся");
  }
  return Promise.reject(error);
});
/**/


//Components
Vue.component('HeaderBlock', require('./components/template/HeaderBlock').default);
Vue.component('SidebarBlock', require('./components/template/SidebarBlock').default);
Vue.component('FooterBlock', require('./components/template/FooterBlock').default);

Vue.use(BootstrapVue);

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
