import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '@/components/Dashboard';
import Collection from '@/components/Collection/Index';
import CollectionForm from '@/components/Collection/Form';
import ProvaiderItem from '@/components/Provaider/ProvaiderItem';
import Collectiontags from '@/components/Collection/CollectionTags';
import Provaider from '@/components/Provaider/Index';
import ProvaiderForm from '@/components/Provaider/Form';
import ProvaiderItemForm from '@/components/Provaider/ProvaiderItemForm';
import Tag from '@/components/Tag/Index';
import TagForm from '@/components/Tag/Form';
import Session from '@/components/Session';
import ProvaiderTags from '@/components/Provaider/ProvaiderTags';
import AccessDenied from '@/components/errors/AccessDenied';
import Auth from '@/components/auth/AuthPage';
import Tasks from '@/components/Tasks/Index';


Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/access_denied',
      component: AccessDenied
    },
    {
      path: '/auth',
      name: 'auth',
      component: Auth
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard,
    },
    {
      path: '/collection',
      name: 'collection',
      component: Collection,
    },
    {
      path: '/collection/create',
      name: 'collection-create',
      component: CollectionForm,
    },
    {
      path: '/collection/:id/edit',
      name: 'collection-edit',
      component: CollectionForm,
    },
    {
      path: '/provaideritem/:id/tags',
      name: 'provaideritem-tags',
      component: ProvaiderTags,
    },
    {
      path: '/collection/:id/tags',
      name: 'collection-tags',
      component: Collectiontags,
    },
    {
      path: '/provaider/:id/items',
      name: 'provaider-item',
      component: ProvaiderItem,
    },
    {
      path: '/provaider',
      name: 'provaider',
      component: Provaider,
    },
    {
      path: '/provaider/create',
      name: 'provaider-create',
      component: ProvaiderForm,
    },
    {
      path: '/provaider/:id/edit',
      name: 'provaider-edit',
      component: ProvaiderForm,
    },
    {
      path: '/provaider/:provaiderid/item/create',
      name: 'provaideritem-create',
      component: ProvaiderItemForm,
    },
    {
      path: '/provaideritem/:id/edit',
      name: 'provaideritem-edit',
      component: ProvaiderItemForm,
    },
    {
      path: '/tag',
      name: 'tag',
      component: Tag,
    },
    {
      path: '/tag/create',
      name: 'tag-create',
      component: TagForm,
    },
    {
      path: '/tag/:id/edit',
      name: 'tag-edit',
      component: TagForm,
    },
    {
      path: '/session',
      name: 'Session',
      component: Session,
    },
    {
      path: '/tasks',
      name: 'tasks',
      component: Tasks,
    },
  ],
});
